#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import datetime
import os

# enable/disable network(s)
# and set (destination path for) output file
IPv4 = True
IPv6 = False
output_file = '/path/to/I2Pd_stats.csv'


# scrape webconsole
I2Pd_webconsole = requests.get('http://127.0.0.1:7070/')
soup = BeautifulSoup(I2Pd_webconsole.text, 'html.parser')

# extract and format data
timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
uptime = soup.find('b', text='Uptime:').nextSibling.string[1:]
tunnel_creation_success_rate = soup.find('b', text='Tunnel creation success rate:').nextSibling.string[1:-1]
received_string = soup.find('b', text='Received:').nextSibling.string[1:]
sent_string = soup.find('b', text='Sent:').nextSibling.string[1:]
transit_string = soup.find('b', text='Transit:').nextSibling.string[1:]
routers = soup.find('b', text='Routers:').nextSibling.string[1:-1]
floodfills = soup.find('b', text='Floodfills:').nextSibling.string[1:-1]
leasesets = soup.find('b', text='LeaseSets:').nextSibling.string[1:]
client_tunnels = soup.find('b', text='Client Tunnels:').nextSibling.string[1:-1]
transit_tunnels = soup.find('b', text='Transit Tunnels:').nextSibling.string[1:]

if IPv4 == True:
    network_status_ipv4 = soup.find('b', text='Network status:').nextSibling.string[1:]
    if IPv6 == True:
        network_status_ipv6 = soup.find('b', text='Network status v6:').nextSibling.string[1:]
    else:
        network_status_ipv6 = "Disabled"
else:
    network_status_ipv4 = "Disabled"
    network_status_ipv6 = soup.find('b', text='Network status v6:').nextSibling.string[1:]

received = received_string.split(' ')[0].replace('.', ',')
received_unit = received_string.split(' ')[1]
received_rate = received_string.split(' ')[2][1:].replace('.', ',')
received_rate_unit = received_string.split(' ')[3][0:-1]
sent = sent_string.split(' ')[0].replace('.', ',')
sent_unit = sent_string.split(' ')[1]
sent_rate = sent_string.split(' ')[2][1:].replace('.', ',')
sent_rate_unit = sent_string.split(' ')[3][0:-1]
transit = transit_string.split(' ')[0].replace('.', ',')
transit_unit = transit_string.split(' ')[1]
transit_rate = transit_string.split(' ')[2][1:].replace('.', ',')
transit_rate_unit = transit_string.split(' ')[3][0:-1]

# write data to file
output = timestamp + ';' + uptime + ';' + received + ';' + received_unit + ';' + received_rate + ';'\
         + received_rate_unit + ';' + sent + ';' + sent_unit + ';' + sent_rate + ';' + sent_rate_unit + ';'\
         + transit + ';' + transit_unit + ';' + transit_rate + ';' + transit_rate_unit + ';'\
         + tunnel_creation_success_rate + ';' + routers + ';' + floodfills + ';' + leasesets + ';'\
         + client_tunnels + ';' + transit_tunnels + ';' + network_status_ipv4 + ';' + network_status_ipv6 + '\n'

if os.path.exists(output_file) == False:
    f = open(output_file, 'x')
    f.write('timestamp;uptime;received;received_unit;received_rate;received_rate_unit;sent;sent_unit;sent_rate;'
            'sent_rate_unit;transit;transit_unit;transit_rate;transit_rate_unit;tunnel_creation_success_rate;'
            'routers;floodfills;leasesets;client_tunnels;transit_tunnels;network_status_ipv4;network_status_ipv6\n')

f = open(output_file, 'a')
f.write(output)
f.close()
