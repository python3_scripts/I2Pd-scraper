# I2Pd-scraper

I2Pd-scraper is a Python3 web-scraper for the [Purple I2P](https://i2pd.website/) Webconsole.

It collects
- Current Time
- Uptime
- Network status
- Tunnel creation success rate
- Received
- Sent
- Transit
- Routers
- Floodfills
- LeaseSets
- Client Tunnels
- Transit Tunnels

and writes the collected data to a CSV-file.

## Installation

Needed Python Moduls are:
- requests
- beautifulsoup4
- datetime
- os
- time

## Usage

There are two scripts
- one for the use with Cron
- one to run in terminal for continuous data scraping

You just have to change your network settings for IPv4 and IPv6 (True/False),
set a (destination path for the) output file, and optionally set
the sleep-intervall if you run the continuous script.

```python
IPv4 = True
IPv6 = False
output_file = 'I2Pd_stats.csv'
sleep = 60
```